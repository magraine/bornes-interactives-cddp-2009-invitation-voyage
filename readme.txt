
{{{Une planche}}}

En partant d'une image (format 1024*768px), il est possible d'en faire une image avec des zones survolables.

 Ajoutez là en tant que document de rubrique. Vous pouvez renseigner en plus dans le champ descriptif du document l'auteur et la source de l'image tel ceci ("_ Source" sera supprimé automatiquement sur les bornes pour ne pas pouvoir suivre le lien, mais pas dans le cas où l'on arriverait par le sommaire) : 
<cadre>
Auteur : Nom Prénom
_ Source : [lien->url]
</cadre>

Ajoutez dans le descriptif de la rubrique (remplacer XX par le numéro du document) :
<cadre>
<imgXX|carte|

>
</cadre>

{{{Les éléments au survol}}}

Chaque élément de l'image qui peut-être survolé (qui affichera alors des images, du texte, du son) seront mis dans des articles de la rubrique-scene.

L'article se compose d'un texte, d'un logo, et éventuellement de sons.
-* Le logo : il doit faire plus de 500px environ pour que l'image de la loupe soit correctement calculée. Il est possible de le dupliquer (en l'insérant en tant que document de l'article) pour ajouter l'auteur et la source dans le descriptif du document (ainsi le document sera montré sur la page des crédits photographiques)
-* les images : elles seront insérées dans le texte avec le modèle image : <code><imageXX|center></code> où XX est l'identifiant du document.
-* les sons : Ils s'ajoutent en tant que document de l'article. Il faudra aussi remplir le champ descriptif avec l'auteur et la source. Tout son dont le titre ne commence pas par "Texte" est lu dès le survol de la zone. Un son commençant par "Texte" est lu si la zone est survolée et si l'on appuie la barre d'espace.

Une fois tout renseigner, publier l'article.

{{{Déterminer les zones}}}

Dans la partie publique, rendez-vous sur la scène dont vous souhaitez définir ou modifier les zones. L'image de fond est donc affichée en grand et le curseur à une forme de loupe (sous Firefox).

-* appuyer simultanément sur "control+alt+r" pour observer les zones déjà définies (actualiser la page pour cacher les zones)
-* appuyer simultanément sur control+alt+z" pour laisser apparaitre une zone.
-* appuyer simultanément sur control+alt+e" pour créer une zone. L'apparence change : le curseur devient un pointeur normal. En cliquant et glissant, vous pouvez sélectionner un espace sur l'image. En haut à gauche, des coordonnées apparaissent. Sélectionner un article, une position du texte (éventuellement un numéro de zone déjà défini, si c'est une modification d'une zone existante) et ajouter (ou remplacer) le texte ainsi généré dans le descriptif de la scène, en double cliquant que "éditer le descriptif" comme ci-dessous. Valider en cliquant "OK".

<cadre>
<img30|carte|

|Goéland marin
|type1=rect
|coord1=30,508,150,598
|lien1=14
|infos1=centre

>
</cadre>

